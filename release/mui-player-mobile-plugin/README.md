<div align="center"><img src="https://muiplayer.oss-cn-shanghai.aliyuncs.com/static/image/logo.png" width="150px"></div>
<div align="center">
    <a href="https://www.npmjs.com/package/mui-player" target="_blank"><img src="https://img.shields.io/badge/npm-v1.4.0-blue" /></a>
    <a href="https://www.npmjs.com/package/mui-player-desktop-plugin" target="_blank"><img src="https://img.shields.io/badge/npm%20player%20desktop-v1.3.0-blue" /></a>
	<a href="https://www.npmjs.com/package/mui-player-mobile-plugin" target="_blank"><img src="https://img.shields.io/badge/npm%20player%20mobile-v1.3.0-blue" /></a>
	<a href="https://github.com/muiplayer/hello-muiplayer/tree/master/dist/js" target="_blank"><img src="https://img.shields.io/badge/gzip%20size-15kb-brightgreen" /></a>
    <a href="https://github.com/muiplayer/hello-muiplayer/blob/master/LICENSE" target="_blank"><img src="https://img.shields.io/badge/license-MIT-brightgreen" /></a>
</div>

<h1 align="center">Mui Player Mobile Plugin</h1>
<a href="https://muiplayer.js.org/" target="_blank">Docs</a> | <a href="https://muiplayer.js.org/zh/" target="_blank">中文文档</a><br><br>

![](https://muiplayer.oss-cn-shanghai.aliyuncs.com/static/image/mobile_preview.png)

## Introduction

mui-player-mobile-plugin.js is an extension plug-in for the player. The plug-in enhances the maneuverability of the player on the mobile terminal, including the touch screen to control the progress and volume, lock the playback, and configure the main menu to switch the playback rate and loop playback. Wait.

## installation

Install using npm:

```
npm i mui-player-mobile-plugin --save
```

Install using yarn:

```
yarn add mui-player-mobile-plugin
```

## usage

Introduce **mui-player-mobile-plugin.js** on the page , the plug-in needs to be loaded before initializing the player:

```html
<!-- Use the script tag to import -->
<script type="text/javascript" src="js/mui-player-mobile-plugin.min.js"></script>
```

```js
// Use the module manager to introduce plugins
import MuiPlayerMobilePlugin from 'mui-player-mobile-plugin'
```

Pass in the main configuration item plugins

```js
var mp = new MuiPlayer({
    container:'#mui-player',
    src:'../media/media.mp4',
    ...
    
    plugins:[
        new MuiPlayerMobilePlugin({
            showMenuButton:true,
            ...
        })
    ]
});
```

## Plugins

- [mui-player-mobile-plugin.js](https://www.npmjs.com/package/mui-player-mobile-plugin)
- [mui-player-desktop-plugin.js](https://www.npmjs.com/package/mui-player-desktop-plugin)